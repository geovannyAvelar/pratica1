# Informações pessoais

Nome: Geovanny de Avelar Carneiro

Matrícula: 212007

E-mail: 1536625@sga.pucminas.br

# Experiência

Sou um programador backend com seis anos de experiência, focado na plataforma 
Java, mas com grande interesse em Go. Atualmente estou trabalhando em 
melhorar minhas habilidades em DevOps.

Durante minha carreira, trabalhei principalmente no desenvolvimento de 
software para o setor público. Meu trabalho mais notável é o WebRADAR, 
uma plataforma web de integração de dados meteorológicos, capaz de efetuar 
fusão de dados meteorológicos de diferentes fontes, em especial radares 
meteorológicos, mas também estações metrológicas de superfície, reportes 
de tempo vindos de aeronaves comerciais, mensagens metrológicas de 
aeródromos, radiossondas e várias outras fontes. O sistema WebRADAR está 
sendo utilizado pelo Departamento de controle do espaço aéreo (DECEA), 
um órgão afiliado à Força Aérea Brasileira, para prover o seu Serviço 
Meteorológico para Aeronave em Voo (VOLMET).
